package com.werapan.oxoop;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class TestReadPlayer {

    public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
        File file = new File("Player.dat");
        FileInputStream fis = new FileInputStream(file);
        ObjectInputStream ois = new ObjectInputStream(fis);
        Player x = (Player) ois.readObject();
        Player o = (Player) ois.readObject();
        ois.close();
        fis.close();
        System.out.println(o);
        System.out.println(x);
    }
}
